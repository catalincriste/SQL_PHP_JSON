package com.vigion.sql;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class AlunoEdit extends AppCompatActivity {

    //Objetos para controlo dos objetos gráficos

    private TextView textViewNproc;
    private EditText editTextNome;
    private EditText editTextUserName;
    private EditText editTextPassword;

    private String nProc; // recebe o valor daa AlunoListView
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aluno_edit);
        editTextNome = (EditText) findViewById(R.id.editTextNomeEdit);
        editTextUserName = (EditText) findViewById(R.id.editTextUserNameEdit);
        editTextPassword = (EditText) findViewById(R.id.editTextPasswordEdit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textViewNproc = (TextView) findViewById(R.id.textViewNproc);
        //Extração do atributo passado pel activty da ListView
        Intent intent = getIntent();
        nProc = intent.getStringExtra(Config.ALUNO_NPROC);
        textViewNproc.setText(textViewNproc.getText() + ": " + nProc); //Atualiza a textview Nproc com nProc
        //Aquisição do ontrolo dos objetos gráficos



        FloatingActionButton fabUpdate = (FloatingActionButton) findViewById(R.id.fabAlunoUpdate);
        fabUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                updateAluno();
            }
        });

        FloatingActionButton fabDelete = (FloatingActionButton) findViewById(R.id.fabAlunoDelete);
        fabDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                confirmDeleteAluno();
            }
        });

        FloatingActionButton fabAlunoList = (FloatingActionButton) findViewById(R.id.fabAlunoList);
        fabAlunoList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                startActivity(new Intent(AlunoEdit.this, AlunoListView.class));
            }
        });
        Log.i("LOG:", "AlunoEdit - onCreate() COMPLETA : Vou Executar getAluno() com nProc: " + nProc);
        getAluno(); //Obtem da base de dados ,o aluno, cujo nProc foi passado no Intent
    }
    private void getAluno(){
        class GetAluno extends AsyncTask<Void,Void,String>{
            ProgressDialog loading; //Cria objecto Progress Bar
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(AlunoEdit.this, "A processar receção de dados...", "Só um momento...",false,false);
            }

            @Override
            protected void onPostExecute(String resposta) {
                super.onPostExecute(resposta);
                loading.dismiss();
                showAluno(resposta);        //Chama o método para mostrar o registo no layout
            }

            @Override
            protected String doInBackground(Void... params) {
                WebConnection rh = new WebConnection();
                Log.i("LOG:", "AlunoEdit - GetAluno() - doInBackground() - WebConnection. sendGstReqquestParam() URL: " + Config.URL_GET_ALUNO + nProc);
                String resposta = rh.sendGetRequest(Config.URL_GET_ALUNO, nProc);
                Log.i("LOG:", "AlunoEdit - GetAluno() - doInBackground() - WebConnection. sendGstRequestParam() Resposta: " + resposta);
                return resposta;
            }

        }
        GetAluno ga = new GetAluno();
        ga.execute();
    }
    private void showAluno(String resposta){
        try{
            Log.i("LOG :", "AlunoEdit - showAluno() - JSON_STRING: " + resposta);

            //Objetos JSON para tratamento dos dados json recebidos na string de parametro

            JSONObject alunoJsonObject = new JSONObject(resposta); //Converte a string para json
            Log.i("LOG :", "AlunoEdit - showAluno() - JSON_STRING alunoJsonObject: " + alunoJsonObject);

            JSONArray result = alunoJsonObject.getJSONArray(Config.TAG_JSON_ARRAY); //separa os elemento JSON, num array
            Log.i("LOG :", "AlunoEdit - showAluno() - result: " + result);

            JSONObject aluno = result.getJSONObject(0);         //Extrai o 1º elemento json array : O aluno
            Log.i("LOG :", "AlunoEdit - showAluno() - JSON Object aluno: " + aluno);

            //Extração dos dados do objecto JSON
            String nome = aluno.getString(Config.TAG_NOME);
            String userName = aluno.getString(Config.TAG_USERNAME);
            String password = aluno.getString(Config.TAG_PASSWORD);

            //Atualização das EditText da activity com os dados extraídos

            editTextNome.setText(nome);
            editTextUserName.setText(userName);
            editTextPassword.setText(password);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.i("LOG:", "AlunoEdit - showAluno() - Exeção: " + e.getMessage());
        }
    }
    private void updateAluno(){
        //Rescolha dos dados das EditText
        final String nome = editTextNome.getText().toString().trim();
        final String userName = editTextUserName.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();

        //Validação de dados ( BD É NOT NULL para estes campos)

        if(nome.isEmpty() || userName.isEmpty() || password.isEmpty()){
            Toast.makeText(AlunoEdit.this,"Campos sem dados ", Toast.LENGTH_LONG).show();
        }
        else{
            //Classe assíncrona para comunicação em Thread a executar em BackGround
            class UpdateEmployee extends AsyncTask<Void,Void,String>
            {
                ProgressDialog loading;
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    loading = ProgressDialog.show(AlunoEdit.this, "A atualizar...", "Esperar...", false,false);
                }

                @Override
                protected void onPostExecute(String resposta) {
                    loading.dismiss();
                    Toast.makeText(AlunoEdit.this, resposta, Toast.LENGTH_LONG).show();
                    startActivity(new Intent(AlunoEdit.this,AlunoListView.class));
                }

                @Override
                protected String doInBackground(Void... params) {
                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put(Config.KEY_NPROC, nProc);
                    hashMap.put(Config.KEY_NOME,nome);
                    hashMap.put(Config.KEY_USERNAME, userName);
                    hashMap.put(Config.KEY_PASSWORD,password);

                    WebConnection rh = new WebConnection();
                    String resposta = rh.sendPostRequest(Config.URL_UPDATE_ALUNO, hashMap);
                    return resposta;
                }
            }
            //Criação do objeto Assincrono e execução
            UpdateEmployee update = new UpdateEmployee();
            update.execute();
        }
    }
    private void deleteAluno(){
        //Classe assincrona para comunicação com o thread a executa em BackGround
        class DeleteEmployee extends AsyncTask<Void,Void,String>{
            ProgressDialog loading;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                loading = ProgressDialog.show(AlunoEdit.this, "Apagar...", "Esperar...", false,false);
            }

            @Override
            protected void onPostExecute(String resposta) {
                super.onPostExecute(resposta);
                loading.dismiss();
                Toast.makeText(AlunoEdit.this, resposta, Toast.LENGTH_LONG). show();
                startActivity(new Intent(AlunoEdit.this,AlunoListView.class));
            }

            @Override
            protected String doInBackground(Void... params) {
                WebConnection rh = new WebConnection();
                String resposta = rh.sendGetRequest(Config.URL_DELETE_ALUNO,nProc);
                return resposta;
            }
        }
        DeleteEmployee de = new DeleteEmployee();
        de.execute();
    }
    private void confirmDeleteAluno(){
        Log.i("LOG:", "Abri Confirm Delete Aluno");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Confirma a elminiação?");

        alertDialogBuilder.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                deleteAluno();
                startActivity(new Intent(AlunoEdit.this, AlunoListView.class));
            }
        });

        alertDialogBuilder.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                //Faz nada

            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

}
