package com.vigion.sql;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;

public class AlunoAdd extends AppCompatActivity {
    //Views
    private EditText editTextNome;
    private EditText editTextUserName;
    private EditText editTextPassword;

    private Button buttonAdd;
    private Button buttonLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aluno_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Aquisição do controlo de objetos gráficos
        editTextNome = (EditText) findViewById(R.id.editTextNome);
        editTextUserName = (EditText) findViewById(R.id.editTextUsername);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);

        //Botões e Listeners
        buttonAdd = (Button) findViewById(R.id.buttonAdd);
        buttonLista = (Button) findViewById(R.id.buttonLista);

        buttonAdd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                addAluno();
                Log.i("BTN ADD", "CLicou no botão");
            }
        });

        buttonLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(),AlunoListView.class));
            }
        });
        //diciona um novo
        FloatingActionButton fabAlunoAdd = (FloatingActionButton) findViewById(R.id.fabAlunoAdd);
        fabAlunoAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addAluno();
            }
        });

        // mostra a lista de alunos
        FloatingActionButton fabAlunoList = (FloatingActionButton) findViewById(R.id.fabAlunoList);
        fabAlunoList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
           //   startActivity(new Intent(AlunoAdd,AlunoListView.class));
                startActivity(new Intent(getApplicationContext(),AlunoListView.class));
            }
        });
        Log.i("LOG:", "AlunoAdd - onCreate() COMPLETA");
    }
    //Adiciona um aluno
    private void addAluno(){
        //Recolha dos dados, inseridos pelo ser
        final String nome = editTextNome.getText().toString().trim();
        final String username = editTextUserName.getText().toString().trim();
        final String password = editTextPassword.getText().toString().trim();
        if(nome.isEmpty() || username.isEmpty() || password.isEmpty()){
            Toast.makeText(AlunoAdd.this, "Campos sem dados", Toast.LENGTH_LONG).show();
        }
        else{


            class AddAluno extends AsyncTask<Void,Void,String>{
                ProgressDialog loading;                 //ProgressBar em Caixa de Mensagem

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    loading = ProgressDialog.show(AlunoAdd.this,"AlunoListView processar envio de dados...", "Só um momento...", false,false);
                }

                @Override
                protected void onPostExecute(String response) {
                    super.onPostExecute(response);
                    loading.dismiss();
                    Toast.makeText(AlunoAdd.this, response, Toast.LENGTH_LONG).show();
                    //startActivity(new Intent(AlunoAdd.this, AlunoListView.class));
                }


                @Override
                protected String doInBackground(Void... params) {
                    //Cria uma collection value pair, de  2 strings : nome de campos e os dados das EditText
                    HashMap<String,String> dados = new HashMap<>();
                    dados.put(Config.KEY_NOME, nome);
                    dados.put(Config.KEY_USERNAME, username);
                    dados.put(Config.KEY_PASSWORD, password);

                    //Comunicação dos dados para Insert, usando o método sendPostRequest() da classe WebConnection
                    WebConnection wc = new WebConnection();      //Cria obj WebConnection
                    Log.i("LOG:", "MainAtivity - Class AddAluno - doInBackground - Vou Chamar WebConnecton.sendPostRequest() com url: " + Config.URL_ADD) ;
                    String resp = wc.sendPostRequest(Config.URL_ADD,dados);       //Envia dados a espera resposta
                    Log.i("LOG:", "MainAtivity - Class AddAluno - doInBackground - Resposta da WebConnection.sendPostRequest() com string: "+resp);

                    return resp;            //Devolve a Resposta
                }
             }
            AddAluno newAluno = new AddAluno();             //Cria um novo objeto Assíncrono
            newAluno.execute();                             //Executa a thread
        }

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
