package com.vigion.sql;

/**
 * Created by Catalin on 19/01/2016.
 */
public class Config {

    //Localização do server dos scriprs PHO
    public static String HOST_PHP_LOCALTION = "http://catalincriste.co.nf/";
  //  public static String HOST_PHP_LOCALTION = "http://10.1.17.81/AndroidSQL/";
    //Endereços dos scripts php
    public static final String URL_ADD = HOST_PHP_LOCALTION + "addAluno.php";
    public static final String URL_GET_ALL = HOST_PHP_LOCALTION +"getAllAlunos.php";
    public static final String URL_GET_ALUNO = HOST_PHP_LOCALTION + "getAluno.php?nProc=";
    public static final String URL_UPDATE_ALUNO = HOST_PHP_LOCALTION + "updateAluno.php";
    public static final String URL_DELETE_ALUNO = HOST_PHP_LOCALTION + "deleteAluno.php?nProc=";

    //Nomes dos atributos a enviar nos Requests , para os scripts php
    public static final String KEY_NPROC = "nProc";
    public static final String KEY_NOME = "Nome";
    public static final String KEY_USERNAME = "UserName";
    public static final String KEY_PASSWORD = "Password";

    //JSON TAGS , a aextrair dos dados remetidos pelos server
    public static final String TAG_JSON_ARRAY = "result";
    public static final String TAG_NPROC = "nProc";
    public static final String TAG_NOME = "Nome";
    public static final String TAG_USERNAME = "UserName";
    public static final String TAG_PASSWORD = "Password";

    // Indetificador do registo a passar  no Intent entre as activities da listView e da edição
    public static final String ALUNO_NPROC = "nProc";
}
