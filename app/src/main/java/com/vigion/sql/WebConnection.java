package com.vigion.sql;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Catalin on 19/01/2016.
 */
public class WebConnection {
    //SendPostRequest()
    /*Executa script php online com nsert, update ou delete : Método POST
    * Envia dados e o nome do script a executar
    * O server recebe a comuniação com o carimbo POST, extraí os dados e insere-os no script,
    * em varáveis definidas para esse efeito
    * Eecuta o script ( insert,update ou delete) e devolve uma resposta : uma string tb do script.
    * HTTPUrlConnection recebe a resposta, que , depois de interpretada e convertida, é mostrada
    *
    * O método  tem dois parametros:
    * 1º - URL do Script php, para onde são enviados os dados (POST Rquest) Pedido de entrega
    * 2º - Collection MashMap(ValuePair) com os dados a enviar com o PostRequest*
    *
    * */

    public String sendPostRequest(String phpURL, HashMap<String,String> postData){
        URL url; //Objecto URL PARA FORMATAR O ENDEREÇO AlunoListView ENVIAR

        Log.i("LOG:", "WebConnection - URL:" + phpURL);

        //StringBuilder para tratar a resposta ao servidor php

        StringBuilder sb = new StringBuilder();
        try{
            url = new URL(phpURL);

            //Cria uma ligação
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            //Configuração da Ligação
            conn.setConnectTimeout(15000); //Limite do tempo para fazer a ligação
            conn.setReadTimeout(15000);    //Limite de tempo para receber resposta do server
            conn.setRequestMethod("POST"); // Tipo de ligação: Post - Entrega de dados a um script
            conn.setDoInput(true);         //Sentido de dados que vai ser proessado
            conn.setDoOutput(true);        //Nesta caso : Saida para Post e entrada como resposta

            OutputStream os = conn.getOutputStream(); //Cria um canal de saída

            //Cria um cais de saída paa escrita de dados e liga-o ao canade de saída.
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

            //Escrita dos dados no cais de saída
            //É necessário convertelos em uma string com o formato url , para simular o Browser
            //convertData2Url() faz a Conversão dos dados da collection para esse formato

            writer.write(convertData2Url(postData));

            writer.flush();          //Comunicação executada
            writer.close();          //Fecho do cais de saída
            os.close();             //Fecho do canal de saída

            int responseCode = conn.getResponseCode();
            if(responseCode == HttpURLConnection.HTTP_OK){

                //Abre cais e canal de entrada para receber resposta resposta Completa do Server Php
                BufferedReader br =  new BufferedReader(new InputStreamReader(conn.getInputStream()));
                sb = new StringBuilder();           //Inicialização da StringBuilder
                String response;                    //String para ler linha a linha
                    //Leitura , linha a linha , da resposta do server, apara uma StringBuilder

                while((response = br.readLine() )!= null){
                    sb.append(response);
                }
            }
        }
         catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch(Exception e){
            e.printStackTrace();;
            Log.i("LOG:", "WebConnection - senPostRequest - Exceção: " + e.getMessage() );
        }
        return sb.toString();   //Devolve a stringbuilder com a resposta do server.
    }
    //sendGetRequest() tem 2 variantes:
    /*
     * - 1 parametro, obtem queries de todos os registos : 1 º parametro é para o sript php
     * - 2 parametros, obtem queries de todos os registos : o 2º é para id
     *
     *
    * */

    /*sendGetRequest(1 parametro)
    Executa script php online com query ara obter registos da BD: Método GET
    Enviar Dados e o nome do script a executar, com o carimbo GET
    O server recebe  comunição, extraí os dados e insere-os no script, em variáveis próprias.
    Executa o script com a query, agora com dados passados e devolve o resultado da query ou erro.
    HttpURLConnection recebe a resposta , com os registos , atráves de conn.getInputStream
        - Pelo canal de entrada inPutStreamReader
        - no cais de entradaa BufferedReader
    Os dados são empilhados na StringBuilder sb e devolvda para quem chamou o método
    O método tem 1 parametro: URL do script php
    * */
    public String sendGetRequest(String phpURL){
        StringBuilder sb = new StringBuilder();
        try{
            URL url = new URL(phpURL);      //Converte a string com a url do script php
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            //Conversão de respota do server ,com o ddos, para uma stringBuilder
            //Lembrar que vem numa string, com formatos de um array json
            String strLine;

            while((strLine = bufferedReader.readLine()) != null){
                sb.append(strLine+"\n");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch(Exception e){
            Log.i("LOG:", "WebConnection - sendGetRequest1 - Excção : " + e.getMessage());
        }
        return sb.toString();           //Devolve
    }

    //sendGetRequest(2 parametros)
    //Executa script php online com query para obter um só regito, indetificando por ID: Método GET
    //Envia dados e o nome do script a executar
    //O server recebe a comunicação com o carimbo GET, estraí os dados e insere-os no script,
    //Em  variáveis  definidas para esse efeito.
    //Executa o script com a query, agora com os dados reais e evolve o resultado da query ou erro.
    //HttpUrlConnection recebe a resposta, com os registos ,através de con.getInputStream
    //  - pelo canal de entrada inPutStreamReder
    //  - no ais de entrada BuferedReader
    //Os dados são empilhados na StringBuilder sb e desenvolvida para quem chamou o método
    //O método tem 2 parametros : URL do sript php e o nProc do registo a obter;

        public String sendGetRequest(String phpURL, String nProc){

        StringBuilder sb = new StringBuilder();
        try{
            URL url = new URL(phpURL+nProc);      //Converte a string com a url do script php
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            //Conversão de respota do server ,com o ddos, para uma stringBuilder
            //Lembrar que vem numa string, com formatos de um array json
            String strLine;

            while((strLine = bufferedReader.readLine()) != null){
                sb.append(strLine+"\n");
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch(Exception e){
            Log.i("LOG:", "WebConnection - sendGetRequest1 - Excção : " + e.getMessage());
        }
        return sb.toString();
    }

    //Método auxiliar para Conversão dos dados a enviar pelo sendPostRequest()
    //Converte os dados a Collection ValuePair recebida, numa string url, como nos browsers
    private String convertData2Url(HashMap<String, String> dados) throws UnsupportedEncodingException {
        StringBuilder dadosConvertidos = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String,String> entry : dados.entrySet()){
            if(first)
            {
                first = false;
            }
            else{dadosConvertidos.append("&");}

            //Converte numa String URL, em que os dados sao pares ligados por "=" e separados por &
            dadosConvertidos.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            dadosConvertidos.append("=");
            dadosConvertidos.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return dadosConvertidos.toString();
    }
}
