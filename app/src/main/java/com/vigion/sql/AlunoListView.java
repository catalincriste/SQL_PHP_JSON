package com.vigion.sql;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AlunoListView extends AppCompatActivity {

    private ListView listView;              //Objeto java para controlo do objeto gráfico ListView
    private String JSON_STRING;             //String para tratamento dos dados JSON

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aluno_listview);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new ListView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Obtém o item com as duas strings1 o 1º é nProc, 2º é o nome
                HashMap<String, String> map = (HashMap) parent.getItemAtPosition(position);

                //Extraiu o nProc do item
                String itemNproc = map.get(Config.TAG_NPROC).toString();

                //Prepara a chamada da activity para exibir os dados do item clicado

                Toast.makeText(getApplicationContext(),"nProc = " + itemNproc + "/n Nome: " + map.get(Config.TAG_NOME).toString(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(AlunoListView.this, AlunoEdit.class);
                intent.putExtra(Config.ALUNO_NPROC, itemNproc);
                startActivity(intent);
            }
        });
      //  getAluno(); //Método com classe assincrona para obter a lista de alunos da BD
        FloatingActionButton fabAlunoRefresh = (FloatingActionButton) findViewById(R.id.fabAlunoRefresh);
        fabAlunoRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Refresh", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                //Refresh da listview
                getALunos();

            }
        });

        FloatingActionButton fabAlunoAdd = (FloatingActionButton) findViewById(R.id.fabAlunoAdd);
        fabAlunoAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              Snackbar.make(view, "Novo Aluno", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                //chama a activity Main Activity
                startActivity(new Intent(AlunoListView.this, AlunoAdd.class));
            }
        });
        Log.i("LOG:", "AlunoListVIew - onCreate() Completa");
    }
    /*
     * getAlunos() - Cria e executa a classe assíncrona para excutar o script php que rescolhe todos o registos
     */
    private void getALunos(){
        class GetAlunos extends AsyncTask<Void,Void,String>{
            ProgressDialog loading; //ria um progressBar em aixa de Dialogo

            @Override
            protected void onPreExecute() {
                loading = ProgressDialog.show(AlunoListView.this, "A processar receção de dados...", "Só um momento...",false,false);
            }

            @Override               //Executa a thread em background no server o script php em método GET e recebe dados ou erro.
            protected String doInBackground(Void... params) {
                WebConnection wc = new WebConnection();
                Log.i("LOG:", "AlunoListView - GETJSON - Chamar WebCOnnection.sendGetRequestN com phoURL:" + Config.URL_GET_ALL);
                String response = wc.sendGetRequest(Config.URL_GET_ALL); //Envia a um getReques com o url da php a executar
                Log.i("LOG:", "AlunoListView - GETJSON - Resposta da WebCOnnection.sendGeRequstN:" + response);
                return response;             //Envia a resposta para o método onPostExecute()
            }
            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);
                loading.dismiss();
                JSON_STRING = response;
                showList(); // Método para Carregar a ListView, com o dados , agora a JSON_STRING
            }

        }
        GetAlunos ga = new GetAlunos();
        ga.execute();
    }

    //showList() extrai os dados json devolvidos pelo métdo getAluno() e contoi a ListView
    private void showList(){
        JSONObject jsonObject = null;           //Processa os dados em formato JSON, devolvidos pelo server php

        //COllection com (COllections ValuePair) para os itens ALuno com nProc e nome
        ArrayList<HashMap<String,String>> listaAlunos = new ArrayList<HashMap<String, String>>();

        try{
            //Tratamento da resposta com os dados, obtida em doInBackground() e armazenada na String constante JSON_STRING
            //Lembra que o server php devolveu os dados dos alunos encontrados na tabela , no formato json . Podia ser em ML

            Log.i("LOG:","AlunoListView . showList() - JSON_STRING: " + JSON_STRING);
            jsonObject = new JSONObject(JSON_STRING); //Converte a string para JSON
            Log.i("LOG:", "AlunoListVIew - showlist() - JSONObject: " + jsonObject);
            JSONArray registosJson = jsonObject.getJSONArray("result");  //separa os elementos JSON , num array
            Log.i("LOG:", "AlunoListView - showLisy() - result: " + registosJson );
            //ApanCha tudo o que estiver na TAG "result", da resposta devolvida pelo Server na JSON_STRING

            //Para cada um dos elemento que estiver no JSONArray result (será um registo da tabela)

            for(int i = 0; i<registosJson.length(); i++){
                JSONObject json = registosJson.getJSONObject(i); //Extrai-o para um objeo JSON
                String nProc = json.getString(Config.TAG_NPROC); //Extraí os atributos esperado
                String name = json.getString(Config.TAG_NOME);

                HashMap<String,String> aluno = new HashMap<>(); //Cria um Collection ValuePair para os dados do Aluno
                aluno.put(Config.TAG_NPROC, nProc);             //Controi a Collection
                aluno.put(Config.TAG_NOME,name);
                listaAlunos.add(aluno);                     //Adiciona-a à lista de Collections listaALnos
                                                            //Tudo isto pode ser feito com 3 Linhas de Código
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.i("LOG:", "AlunoListView - showList() - Exeção: " + e.getMessage());
        }

        ListAdapter adapter = new SimpleAdapter(
                AlunoListView.this,
                listaAlunos,
                R.layout.listview_item,
                new String[]{Config.TAG_NPROC,Config.TAG_NOME},
                new int[]{R.id.nProc, R.id.name});   //COntext - Esta actvity)

        listView.setAdapter(adapter);  //Constroi a ListView
    }
}
